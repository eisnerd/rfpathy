#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <LittleFS.h>
#include <SPI.h>
#include <MFRC522.h>

#include <WebSockets.h>
#include <WebSocketsClient.h>
#include <WebSocketsServer.h>

const char* ssid = "AjtokCsukva";
const char* password = "HogyNeLasson";
const char* hostname = "rfpathy"; // or NULL for no mDNS and default OTA hostname

WiFiManager wifiManager;
uint8 wifiResult;
void setupWifi() {
  //wifiManager.resetSettings();
  wifiManager.setAPStaticIPConfig(IPAddress(10,10,10,10), IPAddress(10,10,10,10), IPAddress(255,0,0,0));
  wifiManager.setHttpPort(8080);
  wifiManager.setConfigPortalBlocking(false);
  wifiManager.autoConnect(hostname? hostname : "ConfigureWiFi");
  int i = 0;
  while((wifiResult = wifiManager.getLastConxResult()) != WL_CONNECTED) {
    Serial.println(wifiManager.getWLStatusString(wifiResult));
    delay(500);
    if (i++ > 5)
      break;
  }
  Serial.print("Wifi network: ");
  Serial.println(wifiManager.getWiFiSSID());
}

void setupOTA() {
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  if (hostname)
    ArduinoOTA.setHostname(hostname);

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
}

WebSocketsServer webSocket(81);
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght);
void setupWebSockets() {
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) { // When a WebSocket message is received
  switch (type) {
    case WStype_DISCONNECTED:             // if the websocket is disconnected
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {              // if a new websocket connection is established
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        //rainbow = false;                  // Turn rainbow off when a new connection is established
      }
      break;
    case WStype_TEXT:                     // if new text data is received
      Serial.printf("[%u] get Text: %s\n", num, payload);
      if (payload[0] == '#') {            // we get RGB data
        uint32_t rgb = (uint32_t) strtol((const char *) &payload[1], NULL, 16);   // decode rgb data
        int r = ((rgb >> 20) & 0x3FF);                     // 10 bits per color, so R: bits 20-29
        int g = ((rgb >> 10) & 0x3FF);                     // G: bits 10-19
        int b =          rgb & 0x3FF;                      // B: bits  0-9

        //analogWrite(LED_RED,   r);                         // write it to the LED output pins
        //analogWrite(LED_GREEN, g);
        //analogWrite(LED_BLUE,  b);
      } else if (payload[0] == 'R') {                      // the browser sends an R when the rainbow effect is enabled
        //rainbow = true;
      } else if (payload[0] == 'N') {                      // the browser sends an N when the rainbow effect is disabled
        //rainbow = false;
      }
      break;
  }
}

ESP8266WebServer server(80);
void handleNotFound();
void setupHttp() {
  /*
  server.on("/",  HTTP_GET, []() {
    server.send(200, "text/plain", ""); 
  });
  */
  server.onNotFound(handleNotFound);
  server.begin();
}

String getContentType(String filename) {
  if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".svg")) return "image/svg+xml";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

bool handleFileRead(String path) { // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";          // If a folder is requested, send the index file
  String contentType = getContentType(path);             // Get the MIME type
  String pathWithGz = path + ".gz";
  if (LittleFS.exists(pathWithGz) || LittleFS.exists(path)) { // If the file exists, either as a compressed archive, or normal
    if (LittleFS.exists(pathWithGz))                         // If there's a compressed version available
      path += ".gz";                                         // Use the compressed verion
    File file = LittleFS.open(path, "r");                    // Open the file
    size_t sent = server.streamFile(file, contentType);    // Send it to the client
    file.close();                                          // Close the file again
    Serial.println(String("\tSent file: ") + path);
    return true;
  }
  Serial.println(String("\tFile Not Found: ") + path);   // If the file doesn't exist, return false
  return false;
}

void handleNotFound() { // if the requested file or page doesn't exist, return a 404 not found error
  if(!handleFileRead(server.uri())){          // check if the file exists in the flash memory (LittleFS), if so, send it
    if (wifiManager.getLastConxResult() != WL_CONNECTED) {
      String portal = "http://10.10.10.10:8080/";//"http://"+WiFi.localIP().toString()+":8080/";
      Serial.print("Sending 302 to ");
      Serial.println(portal);
      server.sendHeader("Location", portal, true);
      server.send ( 302, FPSTR(HTTP_HEAD_CT2), ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
      server.client().stop(); // Stop is needed because we sent no content length
    } else {
      server.send(404, "text/plain", "404: File Not Found");
    }
  }
}

MFRC522 rfid(/*SS*/D4, /*RST*/D8);  // MFRC522 RFID tag reader

void setupMdns() {
  if (hostname) {
    if (MDNS.begin(hostname)) {
      Serial.print("mDNS responder started: http://");
      Serial.print(hostname);
      Serial.println(".local");
      MDNS.addService("http", "tcp", 80);
    } else {
      Serial.println("Error setting up MDNS responder!");
      delay(1000);
      ESP.restart();
    }
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  pinMode(D2, OUTPUT);
  digitalWrite(D2, HIGH);
  delay(500);
  SPI.begin(); // for rfid pcd
  rfid.PCD_Init();
  LittleFS.begin();

  setupWifi();
  setupMdns();
  setupOTA();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  setupHttp();
  setupWebSockets();

  String str = "Files:\r\n";
Dir dir = LittleFS.openDir("/");
while (dir.next()) {
    str += dir.fileName();
    str += " / ";
    str += dir.fileSize();
    str += "\r\n";
}
Serial.print(str+"Done\r\n");
}

class Repeater {
protected:
  unsigned int updateInterval;       // interval between updates
  unsigned long lastUpdate; // last update of position
  int actions;

  virtual void Action() = 0;

  Repeater(unsigned int updateInterval) {
    this->updateInterval = updateInterval;
    this->actions = 0;
  }

public:
  void Update() {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      actions++;
      Action();
      lastUpdate = millis();
    }
  }
};

class TimeBroadcast : public Repeater {
protected:
  String _time;

  virtual void Action() {
    _time = String(millis());
    webSocket.broadcastTXT(_time);
  }

public:
  TimeBroadcast(int updateInterval):
    Repeater(updateInterval) {}
} timeBroadcast(500);

void loop() {
  wifiManager.process();
  uint8 lastResult = wifiResult;
  if (lastResult != WL_CONNECTED && (wifiResult = wifiManager.getLastConxResult()) == WL_CONNECTED) {
    MDNS.notifyAPChange();
    //MDNS.addService("http", "tcp", 80);
    MDNS.end();
    setupMdns();
    //delay(1000);
    //ESP.restart();
  }
  MDNS.update();
  ArduinoOTA.handle();
  server.handleClient();
  webSocket.loop();
  timeBroadcast.Update();

  if (Serial.available() > 0) {
    if (Serial.read() == ' ') {
      wifiManager.resetSettings();
      ESP.restart();
    }
  }

  // Look for new cards
  if (rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial()) {
    byte buffer[64];
    byte bufferSize = 64;
    Serial.print("Card detected at ");
    Serial.println(millis());
    //rfid.PICC_DumpToSerial(&rfid.uid);
    if (rfid.MFRC522::MIFARE_Read((byte)0x7, buffer, &bufferSize) == MFRC522::STATUS_OK) {
      Serial.print("Card read: ");
      Serial.print(bufferSize);
      Serial.print(" ");
      Serial.write(buffer[2]);
      Serial.write(buffer[3]);
      Serial.println();
      webSocket.broadcastTXT(buffer+2, 2);
    }
  }
}
// dump_byte_array(rfid.uid.uidByte, rfid.uid.size);
// Helper routine to dump a byte array as hex values to Serial
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
