var rainbowEnable = false;
var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']);
connection.onopen = function () {
    connection.send('Connect ' + new Date());
};
connection.onerror = function (error) {
    console.log('WebSocket Error ', error);
};
var dot = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100];
var dash = [300, 100, 300, 100, 300, 100, 300, 100];
var suits = "schd";
var pics = "JQKA";
connection.onmessage = function (e) {  
    console.log('Server: ', e.data);
	if (e.data.length == 2) {
		$(".card").html('<img src="/'+e.data+'.svg">');
		Haptics.vibrate(
			[400, 200]
			.concat(dot.concat().splice(0, (suits.indexOf(e.data[0]) + 1)*2))
			.concat(400, 100)
			.concat(pics.indexOf(e.data[1]) < 0? dot.concat().splice(0, e.data[1]*2) : dash.concat().splice(0, (pics.indexOf(e.data[1])+1)*2))
		);
	}
};
connection.onclose = function(){
    console.log('WebSocket connection closed');
};