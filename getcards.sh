#!/bin/bash
(
cd data

for card in Playing_card_{club,spade,heart,diamond}_{A,K,Q,J,2,3,4,5,6,7,8,9,10}; do
	wget "$(wget -qO- https://commons.wikimedia.org/wiki/File:$card.svg|
		grep fullImageLink|
		grep -o 'https://upload[^"]*svg'|
		head -1)"
done

for card in {c,s,h,d}{A,K,Q,J,2,3,4,5,6,7,8,9,x}; do
	n=${card#?}
	mv Playing_card_${card%?}*_${n//x/10}.svg $card.svg
	gzip --best -S.gz $card.svg
done
)
